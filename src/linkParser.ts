export class LinkParser {
  public parseLinkToEmbed(link: string): string {
    let embed: string;
    const source =
      link.startsWith('https://m.youtube.com') ||
      link.startsWith('https://youtube.com') ||
      link.startsWith('https://www.youtube.com')
        ? 'youtube'
        : 'other';
    switch (source) {
      case 'youtube':
        embed = link.replace('watch?v=', 'embed/');
        break;
      case 'other':
        embed = link;
        break;
    }
    return embed;
  }
}
